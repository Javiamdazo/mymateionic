// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyDFHRen1wf5q8cjZjDsdCPfQJ3EqkKpJqY",
  authDomain: "myweight-b504b.firebaseapp.com",
  databaseURL: "https://myweight-b504b.firebaseio.com",
  projectId: "myweight-b504b",
  storageBucket: "myweight-b504b.appspot.com",
  messagingSenderId: "375378938355",
  appId: "1:375378938355:web:700feee05cb20d1551ea86"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
