import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  name:string;
  email:string;
  password:string;

  constructor(private router: Router, private auth: AuthService) { }

  ngOnInit() {
  }

  register(){
    this.auth.register(this.name, this.email, this.password).then(res =>{
      this.router.navigate(['/home']);
    }).catch(err => {
      alert(err)
    });
  }

}
