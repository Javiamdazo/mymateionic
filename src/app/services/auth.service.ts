import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: any;

  constructor(private AFauth: AngularFireAuth) { }

  login(email: string, password: string) {

    return new Promise((resolve, rejected) => {
      this.AFauth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user);
      }).catch(err => {
        rejected(err);
      })
    });
  }

  register(name: string, email: string, password: string) {
    return new Promise((resolve, rejected) => {
      this.AFauth.createUserWithEmailAndPassword(email, password).then(user => {
        this.basicInformationUser(name);
        resolve(user);
      }).catch(err => {
        rejected(err);
      })
    });
  }

  basicInformationUser(name: string) {
    this.AFauth.user.subscribe(user => {
      this.user = user.updateProfile({
        displayName: name,
        photoURL : 'https://picsum.photos/seed/picsum/200/300'
      })
    });
  }

  logOut() {
    this.AFauth.signOut();
  }

  userDetails() {
    return this.AFauth.user;
  }
}
