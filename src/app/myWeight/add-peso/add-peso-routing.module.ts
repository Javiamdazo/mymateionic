import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPesoPage } from './add-peso.page';

const routes: Routes = [
  {
    path: '',
    component: AddPesoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPesoPageRoutingModule {}
