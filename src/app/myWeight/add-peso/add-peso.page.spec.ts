import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddPesoPage } from './add-peso.page';

describe('AddPesoPage', () => {
  let component: AddPesoPage;
  let fixture: ComponentFixture<AddPesoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPesoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddPesoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
