import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPesoPageRoutingModule } from './add-peso-routing.module';

import { AddPesoPage } from './add-peso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPesoPageRoutingModule
  ],
  declarations: [AddPesoPage]
})
export class AddPesoPageModule {}
