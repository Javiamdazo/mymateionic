import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  user : any
  nombre : string
  photoURL : string

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.auth.userDetails().subscribe(user => {
      this.user = user;
      this.nombre = user.displayName;
      this.photoURL = user.photoURL;
      
    });
  }

}
