import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthService } from "../services/auth.service";
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  slideOpts = {
    initialSlide: 1,
    speed: 400,
  };

  slides = [{
    img: 'assets/icon/addPeso.svg',
    button: 'Añadir Peso',
    link: '/add-peso'
  },
  {
    img: 'assets/icon/user.svg',
    button: 'Perfil',
    link: '/perfil'
  },
  {
    img: 'assets/icon/historial.svg',
    button: 'Historial de Pesos',
    link: '/historial'
  }
  ]

  user: any
  titulo :string

  constructor(private menu: MenuController, private auth: AuthService, private router : Router, public actionSheetController: ActionSheetController) { }

  ngOnInit() {
    this.auth.userDetails().subscribe(user => {
      this.user = user;
    });
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  async presentActionSheet() {
    if(this.user){
      const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Cerrar Sesion',
        icon: 'log-out',
        handler: () => {
          this.auth.logOut();
          this.router.navigate(['/login'])
        }
      },
      {
        text: 'Perfil',
        icon: 'person-circle-outline',
        handler: () => {
          console.log("aqui redirige a la pagina del usuario");
        }
      }]
    });
    await actionSheet.present();
    }else{
      const actionSheet = await this.actionSheetController.create({
        header: 'Opciones',
        cssClass: 'my-custom-class',
        buttons: [{
          text: 'Iniciar Sesion',
          icon: 'log-in',
          handler: () => {
            this.router.navigate(['/login'])
          }
        }]
      });
      await actionSheet.present();    
    }
   
    
  }


}
